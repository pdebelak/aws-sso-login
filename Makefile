PYTHON_FILES = $(wildcard aws_sso_cli/*.py)
.PHONY: dist docs deps test release

dist: docs
	rm -f dist/*
	.venv/bin/python setup.py sdist bdist_wheel

test: deps
	.venv/bin/python -m unittest tests/*.py

docs: docs/man/aws-sso-cli.1

docs/man/aws-sso-cli.1: $(PYTHON_FILES) deps
	mkdir -p $(@D)
	.venv/bin/argparse-manpage \
		--pyfile aws_sso_cli/__init__.py \
		--function build_parser \
		--author "Peter Debelak" \
		--author-email "pdebelak@gmail.com" \
		--project-name aws-sso-cli \
		--url "https://gitlab.com/pdebelak/aws-sso-cli" \
		--prog aws-sso-cli >$@

deps: .venv
	.venv/bin/python -m pip install setuptools wheel
	.venv/bin/python -m pip install -e '.[dev]'

.venv:
	python3 -m venv .venv

release:
	./release.sh
