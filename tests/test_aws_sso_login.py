# MIT License

# Copyright (c) 2023 Peter Debelak

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

import os
import subprocess
import tempfile
from base64 import b64encode
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from configparser import ConfigParser
from unittest import TestCase, mock

import botocore.exceptions
from aws_sso_cli import AwsCredentials, AwsDocker, AwsSsoLogin, main


class TestMain(TestCase):
    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_no_args_logged_in(self, mock_session, mock_docker, mock_creds, mock_sso):
        mock_sso.return_value.is_logged_in.return_value = True

        main([])

        mock_session.assert_called_with(profile_name=None)
        mock_sso.return_value.login.assert_not_called()
        mock_creds.assert_not_called()
        mock_docker.assert_not_called()

    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_profile_arg_logged_in(
        self, mock_session, mock_docker, mock_creds, mock_sso
    ):
        mock_sso.return_value.is_logged_in.return_value = True

        main(["--profile", "myprofile"])

        mock_session.assert_called_with(profile_name="myprofile")
        mock_sso.return_value.login.assert_not_called()
        mock_creds.assert_not_called()
        mock_docker.assert_not_called()

    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_no_args_not_logged_in(
        self, mock_session, mock_docker, mock_creds, mock_sso
    ):
        mock_sso.return_value.is_logged_in.return_value = False

        main([])

        mock_sso.return_value.login.assert_called()
        mock_creds.return_value.set_from.assert_called_with(mock_session.return_value)
        mock_docker.return_value.safe_login_from.assert_called_with(
            mock_session.return_value
        )

    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_force_logged_in(self, mock_session, mock_docker, mock_creds, mock_sso):
        mock_sso.return_value.is_logged_in.return_value = True

        main(["--force"])

        mock_sso.return_value.login.assert_called()
        mock_creds.return_value.set_from.assert_called_with(mock_session.return_value)
        mock_docker.return_value.safe_login_from.assert_called_with(
            mock_session.return_value
        )

    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_docker_force_logged_in(
        self, mock_session, mock_docker, mock_creds, mock_sso
    ):
        mock_sso.return_value.is_logged_in.return_value = True

        main(["--docker-force"])

        mock_sso.return_value.login.assert_not_called()
        mock_creds.assert_not_called()
        mock_docker.return_value.safe_login_from.assert_not_called()
        mock_docker.return_value.login_from.assert_called_with(
            mock_session.return_value
        )

    @mock.patch("aws_sso_cli.AwsSsoLogin")
    @mock.patch("aws_sso_cli.AwsCredentials")
    @mock.patch("aws_sso_cli.AwsDocker")
    @mock.patch("aws_sso_cli.boto3.Session")
    def test_docker_force_not_logged_in(
        self, mock_session, mock_docker, mock_creds, mock_sso
    ):
        mock_sso.return_value.is_logged_in.return_value = False

        main(["--docker-force"])

        mock_sso.return_value.login.assert_called()
        mock_creds.return_value.set_from.assert_called_with(mock_session.return_value)
        mock_docker.return_value.safe_login_from.assert_not_called()
        mock_docker.return_value.login_from.assert_called_with(
            mock_session.return_value
        )


class TestAwsSsoLogin(TestCase):
    def test_is_logged_in_unauthorized(self):
        session = mock.MagicMock()
        session.client.return_value.get_caller_identity.side_effect = (
            botocore.exceptions.UnauthorizedSSOTokenError
        )

        sso = AwsSsoLogin(profile=None, session=session)

        self.assertFalse(sso.is_logged_in())
        session.client.assert_called_with("sts")

    def test_is_logged_in_load_error(self):
        session = mock.MagicMock()
        session.client.return_value.get_caller_identity.side_effect = (
            botocore.exceptions.SSOTokenLoadError(error_msg="err")
        )

        sso = AwsSsoLogin(profile=None, session=session)

        self.assertFalse(sso.is_logged_in())

    def test_is_logged_in_no_error(self):
        sso = AwsSsoLogin(profile=None, session=mock.MagicMock())

        self.assertTrue(sso.is_logged_in())

    @mock.patch("aws_sso_cli.subprocess")
    def test_login_no_profile(self, mock_sub):
        sso = AwsSsoLogin(profile=None, session=mock.MagicMock())

        sso.login()

        mock_sub.run.assert_called_with(["aws", "sso", "login"], check=True)

    @mock.patch("aws_sso_cli.subprocess")
    def test_login_with_profile(self, mock_sub):
        sso = AwsSsoLogin(profile="testprofile", session=mock.MagicMock())

        sso.login()

        mock_sub.run.assert_called_with(
            ["aws", "sso", "login", "--profile", "testprofile"], check=True
        )


class TestAwsDocker(TestCase):
    @mock.patch("aws_sso_cli.subprocess.run")
    def test_docker_running_timeout(self, mock_run):
        mock_run.side_effect = subprocess.TimeoutExpired(cmd=["test"], timeout=10)

        self.assertFalse(AwsDocker().docker_running())

    @mock.patch("aws_sso_cli.subprocess.run")
    def test_docker_running_error(self, mock_run):
        mock_run.side_effect = subprocess.CalledProcessError(returncode=1, cmd=["test"])

        self.assertFalse(AwsDocker().docker_running())

    @mock.patch("aws_sso_cli.subprocess.run")
    def test_docker_running_no_error(self, mock_run):
        self.assertTrue(AwsDocker().docker_running())

    @mock.patch("aws_sso_cli.subprocess.run")
    def test_login_from(self, mock_run):
        session = mock.MagicMock()
        session.client.return_value.get_authorization_token.return_value = {
            "authorizationData": [
                {
                    "authorizationToken": b64encode(b"username1:passthefirst").decode(
                        "utf-8"
                    ),
                    "proxyEndpoint": "firstendpoint.com",
                },
                {
                    "authorizationToken": b64encode(b"secondusername:pass2").decode(
                        "utf-8"
                    ),
                    "proxyEndpoint": "endpointthesecond.com",
                },
            ]
        }

        AwsDocker().login_from(session)

        mock_run.assert_has_calls(
            [
                mock.call(
                    [
                        "docker",
                        "login",
                        "--username",
                        "username1",
                        "--password-stdin",
                        "firstendpoint.com",
                    ],
                    input=b"passthefirst",
                    check=True,
                ),
                mock.call(
                    [
                        "docker",
                        "login",
                        "--username",
                        "secondusername",
                        "--password-stdin",
                        "endpointthesecond.com",
                    ],
                    input=b"pass2",
                    check=True,
                ),
            ]
        )

    @mock.patch("aws_sso_cli.sys.stderr")
    def test_safe_login_from_not_running(self, mock_stderr):
        docker = AwsDocker()
        docker.login_from = mock.MagicMock()
        docker.docker_running = mock.MagicMock(return_value=False)

        docker.safe_login_from(mock.MagicMock())

        docker.login_from.assert_not_called()
        mock_stderr.write.assert_has_calls(
            [
                mock.call(
                    "Docker does not appear to be running. Not logging into ECR."
                ),
                mock.call("\n"),
            ]
        )

    @mock.patch("aws_sso_cli.sys.stderr")
    def test_safe_login_from_error(self, mock_stderr):
        docker = AwsDocker()
        docker.login_from = mock.MagicMock(
            side_effect=botocore.exceptions.ClientError(
                error_response={}, operation_name="test"
            )
        )
        docker.docker_running = mock.MagicMock(return_value=True)

        session = mock.MagicMock()
        docker.safe_login_from(session)

        docker.login_from.assert_called_with(session)
        mock_stderr.write.assert_has_calls(
            [
                mock.call(
                    "Got error fetching ECR credentials from AWS: "
                    "An error occurred (Unknown) when calling the test operation: Unknown"
                ),
                mock.call("\n"),
            ]
        )


class TestAwsCredentials(TestCase):
    @mock.patch("aws_sso_cli.os.getenv")
    def test_no_file_exists(self, mock_getenv):
        with tempfile.TemporaryDirectory() as tempdir:
            creds_path = os.path.join(tempdir, "credentials")

            self.assertFalse(os.path.exists(creds_path))

            mock_getenv.return_value = creds_path
            mock_session = mock.MagicMock()
            mock_session.get_credentials.return_value.access_key = "MOCK_KEY"
            mock_session.get_credentials.return_value.secret_key = "MOCK_SECRET_KEY"
            mock_session.get_credentials.return_value.token = None
            mock_session.profile_name = "default"

            with mock.patch.dict(
                os.environ, {"AWS_SHARED_CREDENTIALS_FILE": creds_path}
            ):
                AwsCredentials().set_from(mock_session)

            self.assertTrue(os.path.exists(creds_path))

            parser = ConfigParser()
            parser.read(creds_path)

            self.assertEqual(parser["default"]["aws_access_key_id"], "MOCK_KEY")
            self.assertEqual(
                parser["default"]["aws_secret_access_key"], "MOCK_SECRET_KEY"
            )
            self.assertIsNone(parser["default"].get("aws_session_token"))

    @mock.patch("aws_sso_cli.os.getenv")
    def test_file_exists_empty(self, mock_getenv):
        with tempfile.NamedTemporaryFile() as tmpfile:
            self.assertTrue(os.path.exists(tmpfile.name))

            mock_getenv.return_value = tmpfile.name
            mock_session = mock.MagicMock()
            mock_session.get_credentials.return_value.access_key = "MOCK_KEY"
            mock_session.get_credentials.return_value.secret_key = "MOCK_SECRET_KEY"
            mock_session.get_credentials.return_value.token = "MOCK_TOKEN"
            mock_session.profile_name = "myprofile"

            with mock.patch.dict(
                os.environ, {"AWS_SHARED_CREDENTIALS_FILE": tmpfile.name}
            ):
                AwsCredentials().set_from(mock_session)

            parser = ConfigParser()
            parser.read(tmpfile.name)

            self.assertEqual(parser["myprofile"]["aws_access_key_id"], "MOCK_KEY")
            self.assertEqual(
                parser["myprofile"]["aws_secret_access_key"], "MOCK_SECRET_KEY"
            )
            self.assertEqual(parser["myprofile"]["aws_session_token"], "MOCK_TOKEN")

    @mock.patch("aws_sso_cli.os.getenv")
    def test_file_exists_with_existing(self, mock_getenv):
        with tempfile.NamedTemporaryFile() as tmpfile:
            self.assertTrue(os.path.exists(tmpfile.name))

            parser = ConfigParser()
            parser.read(tmpfile.name)
            parser.add_section("default")
            parser["default"]["aws_access_key_id"] = "DEFAULT_KEY"
            parser["default"]["aws_secret_access_key"] = "DEFAULT_SECRET_KEY"

            parser.add_section("myprofile")
            parser["myprofile"]["aws_access_key_id"] = "OLD_KEY"
            parser["myprofile"]["aws_secret_access_key"] = "OLD_SECRET_KEY"
            parser["myprofile"]["aws_session_token"] = "OLD_TOKEN"

            with open(tmpfile.name, "w") as f:
                parser.write(f)

            mock_getenv.return_value = tmpfile.name
            mock_session = mock.MagicMock()
            mock_session.get_credentials.return_value.access_key = "MOCK_KEY"
            mock_session.get_credentials.return_value.secret_key = "MOCK_SECRET_KEY"
            mock_session.get_credentials.return_value.token = None
            mock_session.profile_name = "myprofile"

            with mock.patch.dict(
                os.environ, {"AWS_SHARED_CREDENTIALS_FILE": tmpfile.name}
            ):
                AwsCredentials().set_from(mock_session)

            parser = ConfigParser()
            parser.read(tmpfile.name)

            self.assertEqual(parser["default"]["aws_access_key_id"], "DEFAULT_KEY")
            self.assertEqual(
                parser["default"]["aws_secret_access_key"], "DEFAULT_SECRET_KEY"
            )
            self.assertIsNone(parser["default"].get("aws_session_token"))
            self.assertEqual(parser["myprofile"]["aws_access_key_id"], "MOCK_KEY")
            self.assertEqual(
                parser["myprofile"]["aws_secret_access_key"], "MOCK_SECRET_KEY"
            )
            self.assertIsNone(parser["myprofile"].get("aws_session_token"))
