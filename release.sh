#!/bin/sh
# MIT License

# Copyright (c) 2023 Peter Debelak

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Script to create a tag and push to ensure a release gets created in
# gitlab. Only works on the main branch and if no outstanding changes
# exist.

set -e

current_branch="$(git rev-parse --abbrev-ref HEAD)"
if ! [ "$current_branch" = main ]; then
  echo "Not on main branch, on $current_branch instead" >&2
  exit 1
fi

if ! [ "$(git status --short 2>/dev/null)" = "" ]; then
  echo "Uncommitted changes found!"
  exit 1
fi

version="v$(python3 -c '
import sys
from unittest.mock import MagicMock
sys.modules["boto3"] = MagicMock()
sys.modules["botocore"] = MagicMock()
sys.modules["botocore.exceptions"] = MagicMock()
import aws_sso_cli
print(aws_sso_cli.__version__)
')"

echo "$version"

git tag "$version"
git push --tags
